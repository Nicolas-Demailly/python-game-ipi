import module, os;

def create(size, position, delay):
    aminatedModule = module.createFromShape(size, [], size, position)
    aminatedModule['delay'] = delay;
    aminatedModule['step'] = 0;
    aminatedModule['pause'] = False;
    module.setFunction(aminatedModule, "tick", tick);
    return aminatedModule;
    
def createFromFile(imgfolder, size, position, delay):
    aminatedModule = create(size, position, delay);
    if (os.path.isfile('data/{}.pix'.format(imgfolder))):
        aminatedModule['image'] = module.getImageFromFile(imgfolder);
        aminatedModule["imageSize"] = module.getImageSize(aminatedModule["image"]);
        if (size == None):
            aminatedModule["displaySize"] = aminatedModule["imageSize"];
    else:
        aminatedModule['images'] = [];
        aminatedModule['played'] = 0;
        i = 0;
        while os.path.isfile('data/{}/{}.pix'.format(imgfolder, str(i))):
            image = module.getImageFromFile('{}/{}'.format(imgfolder, str(i)));
            aminatedModule['images'].append(image);
            i = i + 1;
        module.setFunction(aminatedModule, "animation", multiImageFunc);    
    return aminatedModule;    

def multiImageFunc(self):
    if "played" in self:
        play = self["played"] + 1;
        if play >= len(self["images"]):
            play = 0;
        setImage(self, play);

def setPaused(self, value):
    self['pause'] = value;

def setImage(self, value):
    self["image"] = self["images"][value];
    if 'persistentSize' in self['data']:
        self["imageSize"] = self['data']['persistentSize'];
    else:
        self["imageSize"] = module.getImageSize(self["image"]);
    self['played'] = value;

def tick(self):
    if not self["pause"]:
        new = self["step"] + 1;
        if (new > self["delay"]):
            self["functions"]["animation"](self);
            new = 0;
        self["step"] = new;

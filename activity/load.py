import screen, module, keyboard, glob, os;

buttons = [];

def buttonState(button, state):
    global l;
    if state:
        module.changeColor(button, 'F', 'B');
        while module.getPosition(button)[1] > 110:
            for l in buttons:
                module.setPosition(l, None, module.getPosition(l)[1] - 13);
        while module.getPosition(button)[1] < 30:
            for l in buttons:
                module.setPosition(l, None, module.getPosition(l)[1] + 13);
                    
    else:
        module.changeColor(button, 'B', 'F');
    return True;

def choose(s):
    from activity import game;
    game.loadGame(module.getData(s, 'file'));
    return True;

def init():
    global buttons;
    
    screen.clearModule();
    
    background = module.createFromFile("load/background", None, None);
    hidden = module.createFromFile("load/hidden", None, None);

    i = 30;
    
    buttons.clear();

    for f in glob.glob("save/*.json"):
        mod = module.createFromShape([150, 11], [], [150, 11], [10, i]);
        data = os.path.basename(f).split(".")[0].split("_");
        time = data[1].split("-");
        if data[0] == "?":
            module.addText(mod, 'Introduction', [0, 1], 150, 0, "F");
        else:
            module.addText(mod, '"{}" : Jour {}, {}:{}'.format(data[0], int(time[0]), time[1], time[2]), [0, 1], 150, 0, "F");
        module.setData(mod, "file", f);
        module.setFunction(mod, "interact", choose);
        buttons.append(mod);
        i = i + 13;

    screen.addModule(background);
    
    keyboard.addButtons(buttons, "choose", buttonState);
    
    screen.addModule(hidden);    
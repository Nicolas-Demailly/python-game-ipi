import screen, module, keyboard, animation, sys, time
from activity import settings, load;

running = True;

def logoMove(self):
    y = module.getData(self, "move") + module.getPosition(self)[1];
    if (y == 10):
        module.setData(self, "move", -1);
    if (y == 7):
        module.setData(self, "move", 1);
    module.setPosition(self, None, y);

def buttonState(button, state):
    if state:
        module.setPosition(button, 22, None);
        module.changeColor(button, 'F', 'B');
    else:
        module.setPosition(button, 20, None);
        module.changeColor(button, 'B', 'F');
    return True;

def newGame(button):
    from activity import game;
    game.newGame();
    return True;
    
def loadGame(button):
    keyboard.setBackFunction(init);
    load.init();
    return True;
    
def gameSettings(button):
    keyboard.setBackFunction(init);
    settings.init();
    return True;
    
def exitGame(a=None, b = None):
    global running;
    screen.clearModule();
    running = False;
    time.sleep(.1);
    sys.stdout.write("\033c");
    sys.exit();
    return True;

def init():    
    screen.clearModule();
    
    background = module.createFromFile("menu/background", None, None);
    rain = animation.createFromFile("menu/rain", screen.getGameScreenSize(), None, 1);
    logo = animation.createFromFile("menu/logo", None, [6,6], 5);
   
    module.setData(logo, "move", 1);
    module.setFunction(logo, "animation", logoMove);
    newGameButton = module.createFromFile("menu/button/new_game", None, [20,58]);
    module.setFunction(newGameButton, "interact", newGame);
    loadGameButton = module.createFromFile("menu/button/load_game", None, [20,72]);
    module.setFunction(loadGameButton, "interact", loadGame);
    settingsButton = module.createFromFile("menu/button/settings", None, [20,86]);
    module.setFunction(settingsButton, "interact", gameSettings);
    exitButton = module.createFromFile("menu/button/exit", None, [20,100]);
    module.setFunction(exitButton, "interact", exitGame);

    screen.addModule(background);
    screen.addModule(rain);
    screen.addModule(logo);
    
    keyboard.addButtons([newGameButton, loadGameButton, settingsButton, exitButton], "choose", buttonState);
    
    keyboard.setBackFunction(exitGame);
    
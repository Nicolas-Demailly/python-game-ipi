import screen, module, keyboard, config

def buttonState(button, state):
    if state:
        module.changeColor(button, 'F', 'B');
    else:
        module.changeColor(button, 'B', 'F');
    return True;

fps = None;

def reloadFPS():
    global fps
    fps = module.createFromShape([26, 11], [[0, 0, 100, 100, 'F']], None, [150, 23]);
    module.addText(fps, config.getData("fps"), [5,1], 20, 1, "9");

def defineFPS(a = None):
    global fps
    up = module.createFromFile("fonts/big/37", None, [180, 21]);
    module.changeColor(up, "0", "F");
    center = module.create([[None]], [1,1], [0,0]);
    module.setData(center, "active", True)
    down = module.createFromFile("fonts/big/38", None, [180, 27]);
    module.changeColor(down, "0", "F");
    def fpsChange(button, state):
        if state:
            module.setData(center, "active", True)
            if (button == up):
                config.data['fps'] = config.getData("fps") + 1;
            else:
                config.data['fps'] = config.getData("fps") - 1;
            screen.removeModule(fps);
            reloadFPS();
            buttonState(fps, True);
            screen.addModule(fps);
            return False;
        else:
            return True;
    def fpsValidation(a = None, b = None):
        config.save();
        screen.fps = float(config.getData('fps'));
        init();
        return True;
    module.setFunction(center, "interact", fpsValidation);
    keyboard.addButtons([up, center, down], "choose", fpsChange);

def key(k):
    if (k == '\u001b[a'):
        return "&";
    elif (k == '\u001b[b'):
        return "(";
    elif (k == '\u001b[c'):
        return ")";
    elif (k == '\u001b[d'):
        return "_";
    elif (k == ' '):
        return "?"; 
    else:
        return k;

def init():
    global fps;
    
    screen.clearModule();
    
    background = module.createFromFile("settings/background", None, None);
    
    reloadFPS();
    interact = module.createFromShape([26, 11], [[0, 0, 100, 100, 'F']], None, [150, 60]);
    previousSelection = module.createFromShape([26, 11], [[0, 0, 100, 100, 'F']], None, [150, 72]);
    nextSelection = module.createFromShape([26, 11], [[0, 0, 100, 100, 'F']], None, [150, 84]);
    left = module.createFromShape([26, 11], [[0, 0, 100, 100, 'F']], None, [150, 96]);
    right = module.createFromShape([26, 11], [[0, 0, 100, 100, 'F']], None, [150, 108]);

    module.addText(interact, key(config.getData("keys")['global']['interact']), [9,1], 10, 1, "9");
    module.addText(previousSelection, key(config.getData("keys")['choose']['previous']), [9,1], 10, 1, "9");
    module.addText(nextSelection, key(config.getData("keys")['choose']['next']), [9,1], 10, 1, "9");
    module.addText(left, key(config.getData("keys")['move']['left']), [9,1], 10, 1, "9");
    module.addText(right, key(config.getData("keys")['move']['right']), [9,1], 10, 1, "9");

    module.setFunction(fps, "interact", defineFPS);
    
    def changeKey(mod):
        key = keyboard.getKey();
        if (mod == interact):
            config.data['keys']['global']['interact'] = key;
        if (mod == previousSelection):
            config.data['keys']['choose']['previous'] = key;
        if (mod == nextSelection):
            config.data['keys']['choose']['next'] = key;
        if (mod == left):
            config.data['keys']['move']['left'] = key;
        if (mod == right):
            config.data['keys']['move']['right'] = key;
        config.save();
        init();
        return True;
    
    module.setFunction(interact, "interact", changeKey);
    module.setFunction(previousSelection, "interact", changeKey);
    module.setFunction(nextSelection, "interact", changeKey);
    module.setFunction(left, "interact", changeKey);
    module.setFunction(right, "interact", changeKey);

    screen.addModule(background);
    
    keyboard.addButtons([fps, interact, previousSelection, nextSelection, left, right], "choose", buttonState);
    
import screen, module, keyboard, animation, json, config, random;
from activity import menu, settings, load;

data, tickTime = None, None;

def save(a=None):
    global data
    def delete(mod):
        screen.removeModule(mod);
    mod = animation.create([150, 5], [2,2], 30);
    module.addText(mod, "Partie sauvegardee", [0, 0], 150, 0, "5");
    module.setFunction(mod, "animation", delete);
    screen.addModule(mod);
    day = data['time'][0];
    hour = int(data['time'][1] / 6.25);
    mins = int((data['time'][1] - (6.25 * hour)) * 24);
    if (hour < 9):
        hour = "0" + str(hour);
    if (mins < 9):
        mins = "0" + str(mins);
    with open("save/{}_{}-{}-{}.json".format(data["player"]["name"], day, hour, mins), 'w+', encoding='utf-8') as ffile:
        json.dump(data, ffile);
    return False;

def loadGameFunc(button):
    keyboard.setBackFunction(init);
    load.init();
    return True;
    
def gameSettings(button):
    keyboard.setBackFunction(init);
    settings.init();
    return True;

def returnToMenu(button = None, b = None):
    menu.init();
    return True;

pauseState = False;

pausePanel = module.createFromFile("menu/pause", None, [59, 26]);
saveButton = module.createFromFile("menu/button/save", None, [81, 36]);
module.setFunction(saveButton, "interact", save);
loadGameButton = module.createFromFile("menu/button/load_game", None, [68, 50]);
module.setFunction(loadGameButton, "interact", loadGameFunc);
settingsButton = module.createFromFile("menu/button/settings", None, [91, 64]);
module.setFunction(settingsButton, "interact", gameSettings);
exitButton = module.createFromFile("menu/button/exit", None, [93, 78]);
module.setFunction(exitButton, "interact", returnToMenu);
pauseComponents = [saveButton, loadGameButton, settingsButton, exitButton];
textModule = module.createFromShape(screen.getGameScreenSize(), [], screen.getGameScreenSize(), [0, 0]);
buyModule = module.createFromShape([200, 40], [], [200, 40], [0, 95]);
entityToBuy = None;

def pause(a=None,b=None):
    keyboard.removeListener(keyboard.buttonsListener);
    global pauseState, pauseComponents;
    def choose(button, state):
        if state:
            module.changeColor(button, 'F', 'B');
        else:
            module.changeColor(button, 'B', 'F');
        return True;
    if not pauseState or a == "back":
        pauseState = True;
        screen.addModule(pausePanel);
        for c in pauseComponents:
            choose(c, False);
            screen.addModule(c);
        keyboard.addButtons(pauseComponents, "choose", choose);
    else:
        pauseState = False;
        init();
    return True;

land = [];

player = None;

playerRightImage = animation.createFromFile("game/player/right", [36, 24], [0, 0], 1)
playerLeftImage = animation.createFromFile("game/player/left", [36, 24], [0, 0], 1)

monsterLeftImage = animation.createFromFile("game/entities/monster/0", [14, 23], [0, 0], 1)
monsterRightImage = animation.createFromFile("game/entities/monster/1", [14, 23], [0, 0], 1)

def init():
    global pauseState, land, player;
    
    land.clear();
    screen.clearModule();
    
    if (data["time"][1] >= 240):
        land.append([ module.createFromFile("game/sky/night", [438, 51], [0, 0]), module.createFromFile("game/sky/night", [438, 51], [438, 0]) ]);
    else: 
        land.append([ module.createFromFile("game/sky/day", [438, 51], [0, 0]), module.createFromFile("game/sky/day", [438, 51], [438, 0]) ]);
    land.append([ module.createFromFile("game/montains", [438, 61], [0, 17]), module.createFromFile("game/montains", [438, 61], [438, 17]) ]);
    land.append([ module.createFromFile("game/land", [876, 53], [0, 25]), module.createFromFile("game/land", [876, 53], [876, 25]) ]);
    land.append([ animation.createFromFile("game/river", [219, 44], [0, 78], 6), animation.createFromFile("game/river", [219, 44], [219, 78], 6)]);

    for a in land:
        animation.multiImageFunc(a[0]);
        animation.multiImageFunc(a[1]);
        screen.addModule(a[0]);
        screen.addModule(a[1]);
    
    for e in data["entities"]:
        e["module"]["hiden"] = True;
        screen.addModule(e["module"]);
    
    player = animation.createFromFile("game/player/right", [36, 24], [90, 55], 1)
    module.setData(player, "persistentSize", [36, 12]);
    animation.setPaused(player, True);
    animation.setImage(player, 5);
    
    screen.addModule(player);
    
    keyboard.addListener(config.getData("keys")['move']['left'], move, -1);
    keyboard.addListener(config.getData("keys")['move']['right'], move, 1);

    screen.addModule(textModule);
    screen.addModule(buyModule);

    if (pauseState):
        pause("back");
    else:
        tickModule = module.createFromShape([1, 1], [], [1, 1], [1,1]);
        tickModule["hidden"] = True;
        module.setFunction(tickModule, "tick", tick);
        screen.addModule(tickModule);

    move(None, 0);

    keyboard.addListener(config.getData("keys")["global"]["interact"], buy);
    keyboard.setBackFunction(pause);
    
def tick(a=None):
    global pauseState, tickTime, player, textModule, data, entityToBuy;
    if not pauseState:
        module.clear(textModule);
        animation.setPaused(player, True);
        tickTime = tickTime + 1;
        
        temp = - module.getPosition(player)[0] + data['player']['position'];
        
        screenDisplay = [temp, temp + screen.getGameScreenSize()[0]];
        module.clear(buyModule);
        entityToBuy = None;
        for e in data["entities"]:
            if (e["type"] == "monster"):
                if (e["specs"]["back"]):
                    if (e["position"] > 0):
                        e["position"] = e["position"] + 8;
                        e["module"]["images"] = monsterRightImage["images"];
                    else:
                        e["position"] = e["position"] - 8;
                        e["module"]["images"] = monsterLeftImage["images"];
                    if (abs(e["position"]) >= config.getData("game")["world_size"] / 2 + 20):
                        screen.removeModule(e);
                        data["entities"].remove(e);
                elif (e["position"] >= data["player"]["position"]):
                    e["position"] = e["position"] - 8;
                    e["module"]["images"] = monsterLeftImage["images"];
                else:
                    e["position"] = e["position"] + 8;
                    e["module"]["images"] = monsterRightImage["images"];
            
            moduleSize = module.getSize(e["module"])[0];
            moduleDisplay = [e["position"], e["position"] + moduleSize];
            
            if colide(screenDisplay, moduleDisplay):
                e["module"]["hidden"] = False;
                
                modulePosition = [moduleDisplay[0] - screenDisplay[0], int(screen.getGameScreenSize()[1] - module.getSize(e["module"])[1] * 2) - 36];
                
                module.setPosition(e["module"], modulePosition[0], modulePosition[1]);
            
                playerPosition = module.getPosition(player)[0];
                playerDisplay = [playerPosition, playerPosition+ module.getSize(player)[0]];
                moduleDisplay = [modulePosition[0], modulePosition[0] + moduleSize];
                
                if (e["type"] == "monster"):
                    for e2 in data["entities"]:
                        if (e2["type"] == "barrier" or e2["type"] == "castle" or e2["type"] == "farm") and not e2["level"] == "0":
                            t = False;
                            if (e2["type"] == "farm"):
                                e2["specs"]["money"] = 0;
                            elif (e["module"]["images"] == monsterLeftImage["images"]):
                                if (e["position"] <= (e2["position"] + module.getSize(e2["module"])[0])):
                                    e["position"] = e["position"] + 8;
                                    t = True;
                            else:
                                if (e["position"] >= e2["position"]):
                                    e["position"] = e["position"] - 8;
                                    t = True;
                            if t:
                                if (e["module"]["images"] == monsterLeftImage["images"]):
                                    e["position"] = e["position"] + 8;
                                else:
                                    e["position"] = e["position"] - 8;
                                e2["specs"]["life"] = e2["specs"]["life"] - random.randint(5, 20);
                                if (random.randint(0, int(3 + (e2["specs"]["peaple"] / 2))) < e2["specs"]["peaple"]):
                                    screen.removeModule(e["module"]);
                                    data["entities"].remove(e);
                                    data["player"]["money"] = data["player"]["money"] + random.randint(1, 2);
                                if (e2["specs"]["life"] <= 0):
                                    lvl = int(e2["level"]) - 1;
                                    module.changeImage(e2["module"], module.getImageFromFile("game/entities/" + e2["type"] + "/" + str(lvl)));
                                    e2["level"] = str(lvl);
                                    temp = 0;
                                    if ("peaple" in e2["specs"]):
                                        temp = int(e2["specs"]["peaple"]);
                                    e2["specs"] = dict(config.getData("game")["specs"][e2["type"]][e2["level"]]);
                                    if ("peaple" in e2["specs"]):
                                        e2["specs"]["peaple"] = temp;
                
                if ((playerDisplay[0] >= moduleDisplay[0] and playerDisplay[1] <= moduleDisplay[1]) or
                    (moduleDisplay[0] >= playerDisplay[0] and moduleDisplay[1] <= playerDisplay[1])):
                    showBuy(e);
                    if (e["type"] == "monster" and not e["specs"]["back"]):
                        e["specs"]["back"] = True;
                        if data["player"]["money"] == 0:
                            pauseState = True;
                            module.clear(buyModule);
                            strb = "Le roi est mort";
                            keyboard.setBackFunction(returnToMenu);
                            screen.removeModule(player);
                            module.addText(buyModule, strb, [int(screen.getGameScreenSize()[0] / 2 - len(strb) * 3) , 10], 200, 0, color="1");
                            return;
                        else:
                            data["player"]["money"] = data["player"]["money"] - 1;
                    if ("peaple" in e["specs"]):
                        strb = "Population : " + str(e["specs"]["peaple"]);
                        module.addText(buyModule, strb, [int(screen.getGameScreenSize()[0] / 2 - len(strb) * 3) , 20], 200, 0, color="0");
            
            else:
                e["module"]["hidden"] = True;       
        
        if (data['time'][0] == 0):
            if (data['player']['name'] == "?"):
                if (tickTime < 40):
                    if (tickTime >= 5):
                        module.addText(textModule, "Encore une fois,", [60 , 5], 150, 0, color="0");
                    if (tickTime >= 20):
                        module.addText(textModule, "le roi esseyera de survivre...", [20 , 16], 150, 0, color="0");
                else:
                    if (tickTime >= 45):
                        module.addText(textModule, "Mais avant tout,", [60 , 5], 150, 0, color="0");
                    if (tickTime >= 60):
                        module.addText(textModule, "un roi doit avoir un nom", [30 , 16], 150, 0, color="0");
                        module.addText(textModule, "Votre nom : " + data["player"]["temp"], [int(screen.getGameScreenSize()[0] / 2 - ((7 + len(data["player"]["temp"])) * 8) / 2), 27], 150, 0, color="B");
                        if not keyboard.hasListener("all"):
                            keyboard.addListener("all", composeName);
            elif data["entities"][0]["level"] == "0":
                if (tickTime >= 5):
                    module.addText(textModule, "Un roi ne vaut rien sans son chateau...", [2 , 5], 200, 0, color="0");
                if (tickTime >= 20):
                    module.addText(textModule, "Ameliorer le chateau pour commencer" , [5 , 16], 200, 0, color="B");
                    showBuy(data["entities"][0]);
            else:
                if (tickTime <= 50):
                    if (tickTime >= 5):
                        module.addText(textModule, "Vous etes roi, recrutez, ameliorez" , [7 , 5], 200, 0, color="0");
                    if (tickTime >= 20):
                        module.addText(textModule, "mais surtout battez vous" , [40 , 16], 200, 0, color="0");
                else:
                    data["time"][0] = 1;
            return;
        elif tickTime >= screen.fps:
            tickTime = 0;
            passSec();
        clr = "0";
        if (data['time'][1] >= 90):
            clr = "7";
            
        module.addText(textModule, "Jour " + str(data['time'][0]), [2 , 2], 100, 0, color=clr);
        module.addText(textModule, str(data['player']["money"]), [int(screen.getGameScreenSize()[0] - len(str(data['player']["money"])) * 6) - 2 , 2], 50, 0, color="B");     
                
def passSec():
    global data, land;        
    data['time'][1] = data['time'][1] + 1;
    if (data['time'][1] == 90):
        for i in land[0]:
            module.changeImage(i, module.getImageFromFile("game/sky/night"));
    day = data['time'][0];
    if (data['time'][1] >= 93 and (data['time'][1] - 93) < (((1/3) * day * day) + day + 1)):
        rand = random.randint(0, 1);
        summonMonster(rand);
    if (data['time'][1] >= 150):
        summonVillages();
        data['time'][1] = 0;
        if data['time'][0] > 0:
            for i in land[0]:
                module.changeImage(i, module.getImageFromFile("game/sky/day"));
            passDay();
    for e in data['entities']:
        if "give" in e["specs"]:
            e["specs"]["temp"] = e["specs"]["temp"] + 1;
            if (e["specs"]["temp"] >= e["specs"]["give"]):
                e["specs"]["temp"] = 0;
                e["specs"]["money"] = e["specs"]["money"] + 1;

def passDay():
    global data;
    data['time'][0] = data['time'][0] + 1;

def summonVillages():
    global data;
    for e in data["entities"]:
        if (e['type'] == 'village'):
            level = str(random.randint(1, 3));
            e["level"] = level;
            e["specs"] = config.getData("game")["specs"]["village"][level];

def summonMonster(where):
    global data, config;
    portal = data["entities"][where + 1];
    entity = animation.createFromFile("game/entities/monster/" + str(where), [14, 23], [90, 63], 1)
    module.setData(entity, "persistentSize", [14, 23/2]);
    screen.addModule(entity);
    data["entities"].append({
        "type": "monster",
        "module": entity,
        "position": portal["position"],
        "specs": dict(config.getData("game")["specs"]["monster"]["0"]),
        "hidden":  True
    });

def composeName(key, d):
    global data, tickTime;
    if (key in list("azertyuiopqsdfghjklmwxcvbn ") and len(data["player"]["temp"]) < 20):
        data["player"]["temp"] = data["player"]["temp"] + key;
    elif (key == "\u007f"):
        data["player"]["temp"] = data["player"]["temp"][:-1];
    elif (key == config.getData("keys")["global"]["interact"]):
        data["player"]["name"] = data["player"]["temp"];
        keyboard.removeListener(composeName);
        tickTime = 0;

def move(key, direction):
    global data, land, player;
    if pauseState:
        return;
    
    if ((data['player']['position'] >= (config.getData("game")["world_size"] / 2 - 36) and direction > 0)
        or (data['player']['position'] <= (- config.getData("game")["world_size"] / 2 + 20) and direction < 0)):
        return;
    
    data['player']['position'] = data['player']['position'] + direction;
    
    pp = module.getPosition(player)[0];
    pp = direction + pp;
    
    if not direction == 0:
        animation.setPaused(player, False);
        if (direction == 1):
            player['images'] = playerRightImage['images'];
        else:
            player['images'] = playerLeftImage['images'];
        
    if ((direction == 1 and pp > 50) or (direction == -1 and pp < 133)):
        slow = 4;
        for a in land:
            slow = slow - 1;
            if "slow" in a[0]:
                a[0]["slow"] = a[0]["slow"] + 1;
            else:
                a[0]["slow"] = 0;
            if (a[0]["slow"] >= slow):
                a[0]["slow"] = 0;
                maxX = module.getSize(a[0])[0];
                x = module.getPosition(a[0])[0] - direction*2;
                if x > 0:
                    x = x - maxX
                if x < -maxX:
                    x = x + maxX;
                module.setPosition(a[0], x, None);
                module.setPosition(a[1], x + maxX, None);
        module.setPosition(player, module.getPosition(player)[0] - direction, None);
    else:
        module.setPosition(player, pp, None);

def colide(a, b):
    return ((a[0] <= b[0] and b[0] <= a[1]) or 
            (a[0] <= b[1] and b[1] <= a[1]) or 
            (b[0] <= a[0] and a[0] <= b[1]) or 
            (b[0] <= a[1] and a[1] <= b[1]));

def showBuy(entity):
    global buyModule, entityToBuy;
    if data["player"]["name"] == "?":
        return;
    if (entity["type"] == "farm"):
        data["player"]["money"] = data["player"]["money"] + entity["specs"]["money"];
        entity["specs"]["money"] = 0;
    if ("cost" in entity["specs"]):
        entityToBuy = entity;
        strb = "";
        for i in range(entity["specs"]["cost"]):
            if (entity["buy"] > i):
                strb = strb + " *";
            else:
                strb = strb + " O";
        module.addText(buyModule, strb, [int(screen.getGameScreenSize()[0] / 2 - len(strb) * 3 - 3) , 10], 200, 0, color="B");
        if not "next" in entity["specs"] and entity["type"] == "castle":
            strb = "declancher un raid";
            module.addText(buyModule, strb, [int(screen.getGameScreenSize()[0] / 2 - len(strb) * 3) , 0], 200, 0, color="1");


def buy(key, d):
    global data, tickTime, pauseState;
    entity = entityToBuy;
    if (pauseState or entityToBuy == None or (data["time"][0] == 0 and not entityToBuy["type"] == 'castle') or data["player"]["money"] <= 0 or not "cost" in entity["specs"]):
        return;
    module.clear(buyModule);
    entity["buy"] = entity["buy"] + 1;
    data["player"]["money"] = data["player"]["money"] - 1;
    if (entity["buy"] >= entity["specs"]["cost"]):
        if ("next" in entity["specs"]):
            if (entity["type"] == "village"):
                for i in range(0, int(entity["level"])):
                    a = False;
                    while not a:
                        r = random.randint(0, len(data["entities"]) - 1);
                        e = data["entities"][r];
                        if ("peaple" in e["specs"]):
                            e["specs"]["peaple"] = e["specs"]["peaple"] + 1;
                            a = True;
                
            module.changeImage(entity["module"], module.getImageFromFile("game/entities/" + entity["type"] + "/" + entity["specs"]["next"]));
            entity["level"] = entity["specs"]["next"];
            temp = 0;
            if ("peaple" in entity["specs"]):
                temp = int(entity["specs"]["peaple"]);
            entity["specs"] = dict(config.getData("game")["specs"][entity["type"]][entity["level"]]);
            if ("peaple" in entity["specs"]):
                entity["specs"]["peaple"] = temp;
            if (data['time'][0] == 0):
                tickTime = 0;
        elif (entity["type"] == "castle"):
            r = random.randint((+ (data["time"][0] - 2)) * 5, data["time"][0] * 5);
            n = 0;
            for e in data["entities"]:
                if "peaple" in e["specs"]:
                    n = n + e["specs"]["peaple"];
            strb = "le raid a echouer..."
            if (n >= r):
                strb = "un portail a ete detruit";
                if (data["entities"][1]["broken"]):
                    module.clear(buyModule);
                    pauseState = True;
                    strb = "Vous avez gagne";
                    keyboard.setBackFunction(returnToMenu);
                    module.addText(buyModule, strb, [int(screen.getGameScreenSize()[0] / 2 - len(strb) * 3) , 10], 200, 0, color="1");
                else:
                    def delete(mod):
                        screen.removeModule(mod);
                    mod = animation.create([200, 40], [0, 9], 30);
                    module.setFunction(mod, "animation", delete);
                    module.addText(mod, strb, [int(screen.getGameScreenSize()[0] / 2 - len(strb) * 3) , 10], 200, 0, color="1");
                    screen.addModule(mod);
                    module.changeImage(data["entities"][1]["module"], module.getImageFromFile("game/entities/portal/leftb"));
                    data["entities"][1]["broken"] = True;
            else:
                def delete(mod):
                    screen.removeModule(mod);
                mod = animation.create([200, 40], [0, 9], 30);
                module.setFunction(mod, "animation", delete);
                module.addText(mod, strb, [int(screen.getGameScreenSize()[0] / 2 - len(strb) * 3) , 10], 200, 0, color="1");
                screen.addModule(mod);
            entity["buy"] = 0;
            return;
        entity["buy"] = 0;

def loadGame(saveFile):
    global pauseState, data, tickTime;
    pauseState = True;
    tickTime = 0;
    with open(saveFile, encoding='utf-8') as ffile:
        data = json.loads(ffile.read());
    init();
    
def newGame():
    global pauseState, data, tickTime;
    pauseState = False;
    tickTime = 0;
    gameConfig = config.getData("game");
    entities = [
        {
            "type": "castle",
            "module": module.createFromFile("game/entities/castle/0", None, [0, 0]),
            "position" : -154,
            "level": "0",
            "specs": dict(gameConfig["specs"]["castle"]["0"]),
            "buy": 0,
            "hidden":  True
        },
        {
            "type": "portal",
            "module": module.createFromFile("game/entities/portal/left", None, [0, 0]),
            "position": int(- gameConfig["world_size"] / 2),
            "specs": dict(gameConfig["specs"]["portal"]["0"]),
            "level": "0",
            "hidden":  True,
            "broken": False
        },
        {
            "type": "portal",
            "module": module.createFromFile("game/entities/portal/right", None, [0, 0]),
            "position": int(gameConfig["world_size"] / 2),
            "specs": dict(gameConfig["specs"]["portal"]["0"]),
            "level": "0",
            "hidden":  True,
            "broken": False
        }
    ];
    # World generation
    for key, value in gameConfig["entities"].items():
        for i in range(0, value):
            a = True
            x = None;
            mod = None;
            while a:
                a = False;
                x = random.randint(- gameConfig['world_size'] / 2, gameConfig['world_size'] / 2);
                mod = module.createFromFile("game/entities/" + key + "/0", None, [0, 0]);
                screenDisplay = [x, x + module.getSize(mod)[0]];
                for e in entities:
                    es = module.getSize(e["module"])[0];
                    ex = e["position"];
                    
                    moduleDisplay = [ex, ex + es];
                    if colide(moduleDisplay, screenDisplay):
                        a = True;
            entities.append({
                "type": key,
                "module": mod,
                "position": x,
                "level": "0",
                "buy": 0,
                "specs": dict(gameConfig["specs"][key]["0"]),
                "hidden":  True
            })
            
    data = {
        "time" : [0, 0],
        "player" : {
            "name" : "?",
            "temp" : "",
            "position": 0,
            "money": 12
        },
        "entities" : entities
    };
    init();
    summonVillages();
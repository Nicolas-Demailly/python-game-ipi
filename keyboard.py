import threading, config, module, screen
from getkey import getkey

listeners = {};
backFunction = None;

def back(a=None,b=None):
    global backFunction;
    if not backFunction == None:
        backFunction();

def setBackFunction(func):
    global backFunction;
    backFunction = func;

def addListener(key, func, data = None):
    global listeners;
    key = str.lower(key);
    if not key in listeners:
        listeners[key] = {};
    listeners[key][func] = data;

def hasListener(key):
    global listeners;
    return key in listeners;

def removeListener(func, firstData = None):
    global listeners;
    for key in listeners:
        if func in listeners[key].keys():
            if firstData == None or firstData == listeners[key][func][0]:
                del listeners[key][func];
    
def clearListeners(key):
    global listeners;
    if (key in listeners):
        del(listeners[key]);

def getKey():
    return str.lower(getkey())

def keyboardThread():
    global listeners;
    while True:
        key = getKey();
        if key in listeners:
            aa = list(listeners[key]);
            aa.reverse();
            for func in aa:
                if not func(key, listeners[key][func]):
                    break;
            del aa;
        if "all" in listeners:
            aa = list(listeners["all"]);
            aa.reverse();
            for func in aa:
                if not func(key, listeners["all"][func]):
                    break;
            del aa;

def buttonsListener(key, data):
    buttons = data[0];
    if len(data) == 1:
        index = 0;
    elif data[1]:
        index = 1;
    else:
        index = -1;
    for button in data[0]:
        if module.getData(button, "active"):
            if (len(data) == 1):
                if button['functions']['interact'](button):
                    removeListener(buttonsListener, buttons); 
                return;
            if not data[2](button, False):
                return;
            module.setData(button, "active", False);
            break;
        else:
            index = index + 1;
    if len(data) == 1:
        return False;
    elif index < 0:
        index = len(buttons) - 1;
    elif index >= len(buttons):
        index = 0;
    active = buttons[index];
    if (data[2](active, True)):
        module.setData(active, "active", True);
    return False;
 
def addButtons(buttons, configname, function):
    for i in buttons:
        module.setData(i, "active", False);
        screen.addModule(i);
    addListener(config.getData("keys")[configname]['previous'], buttonsListener, [buttons, False, function]);
    addListener(config.getData("keys")[configname]['next'], buttonsListener, [buttons, True, function]);
    addListener(config.getData("keys")['global']['interact'], buttonsListener, [buttons]);
    
thread = threading.Thread(target = keyboardThread);
thread.start();

addListener("\x1b", back);

    
import json;

configFile = "data/config.json";
data = {};

def load():
    global data, configFile;
    with open(configFile, encoding='utf-8') as ffile:
        data = json.loads(ffile.read());
    
def save():
    with open(configFile, 'w', encoding='utf-8') as ffile:
        json.dump(data, ffile);

def getData(name):
    global data;
    return data[name];

load();

def getImageSize(image):
    width = 0;
    for l in image:
        if width < len(l):
            width = len(l);
    height = len(image) / 2;
    return [width, height];

def create(image, size, position):
    if size == None:
        size = getImageSize(image);
    else:
        size = [size[0], size[1] / 2]
    if position == None:
        position = [0, 0];
    return {
        "image" : image,
        "imageSize" : getImageSize(image),
        "displaySize" : size,
        "position" : position,
        "data" : {},
        "functions" : {}
    };

def changeImage(self, image, size = None):
    if size == None:
        size = getImageSize(image);
    else:
        size = [size[0], size[1] / 2]
    self["image"] = image;
    self["imageSize"] = size;

def getImageFromFile(imgfile):
    image = [];
    with open("data/{}.pix".format(imgfile)) as fp:
        for line in fp:
            il = [];
            for ch in line.rstrip():
                if (ch == " "):
                    il.append(None);
                else:
                    il.append(ch);
            image.append(il);
    return image
    
def createFromFile(imgfile, size, position):
    image = getImageFromFile(imgfile);
    return create(image, size, position);

def createFromShape(globalSize, shapes, size, position):
    if (globalSize == None):
        return create([], size, position);
    image = [];
    xSize = float(globalSize[0]);
    ySize = float(globalSize[1]);
    for y in range(0, globalSize[1]):
        line = [];
        for x in range(0, globalSize[0]):
            pixel = None;
            for shape in shapes:
                xMin = xSize * (float(shape[0]) / 100.);
                xMax = xSize * (float(shape[0] + shape[2]) / 100.);
                yMin = ySize * (float(shape[1]) / 100.);
                yMax = ySize * (float(shape[1] + shape[3]) / 100.);
                if (x >= xMin and x <= xMax and y >= yMin and y <= yMax):
                    pixel = shape[4];
            line.append(pixel)
        image.append(line);
    return create(image, size, position);

def getPixel(self, x, y):
    x = int(float(x) * (float(self["imageSize"][0]) / float(self["displaySize"][0]))) - self["position"][0];
    y = int(float(y) * (float(self["imageSize"][1]) / float(self["displaySize"][1]))) - self["position"][1];
    if (x < 0 or y < 0 or x >= self["imageSize"][0] or y >= self["imageSize"][1]*2 or y >= len(self["image"])):
        return None;
    line = self["image"][y];
    if (x >= len(line)):
        return None;
    return line[x];

def setFunction(self, name, value):
    self['functions'][name] = value;

def getPosition(self):
    return self['position'];

def getSize(self):
    return self['imageSize'];

def setPosition(self, x, y):
    if not x == None:
        self['position'][0] = x;
    if not y == None:
        self['position'][1] = y;

def getData(self, name):
    return self['data'][name];

def setData(self, name, value):
    self['data'][name] = value;
    
def changeColor(self, old, new):
    for y in range(0, int(self['imageSize'][1]*2)):
        for x in range(0, self['imageSize'][0]):
            line = self['image'][y]
            if len(line) > x and line[x] == old:
                self['image'][y][x] = new;

def clear(self):
    for y in range(0, int(self['imageSize'][1]*2)):
        for x in range(0, self['imageSize'][0]):
            self['image'][y][x] = None;

list = 'a;b;c;d;e;f;g;h;i;j;k;l;m;n;o;p;q;r;s;t;u;v;w;x;y;z;0;1;2;3;4;5;6;7;8;9;?;&;(;);_;,;.;:;";*'.split(";");
characters = { " " : None }
for i in range(0, len(list)):
    characters[list[i]] = [getImageFromFile("fonts/little/%d" % i), getImageFromFile("fonts/big/%d" % i)]
del list;

def addText(self, text, position, size, font = 0, color = "0"):
    splittedtext = [];
    actual = "";
    for part in str.lower(str(text)).split(" "):
        if ((len(part) - 1) * (5 + font * 2)) <= size - (((len(actual) - 1) * (5 + font * 2)) - 1):
            actual = actual + part + " ";
        else:
            splittedtext.append(actual);
            actual = part + " ";
    splittedtext.append(actual);
    textImage = [];
    for part in splittedtext:
        for i in range(0, (5 + font * 4)):
            line = [];
            for c in part:
                for a in range(0, (5 + font * 2)):
                    if (not c in characters):
                        print(c);
                        tt = characters['?'][font][i][a];
                        if not tt == None:
                            tt = color;
                    elif (c == ' ' or len(characters[c][font][i]) <= a):
                        tt = None;
                    else:
                        tt = characters[c][font][i][a];
                        if not tt == None:
                            tt = color;
                    line.append(tt)
                line.append(None);
            textImage.append(line);
        textImage.append([None]);
    for y in range(position[1], int(self['imageSize'][1]*2)):
        for x in range(position[0], self['imageSize'][0]):
            line = self['image'][y];
            realY = y - position[1];
            if len(line) > x and len(textImage) > realY:
                realX = x - position[0];
                ll = textImage[realY];
                if (len(ll) > realX and ll[realX] != None):
                    self['image'][y][x] = ll[realX];
                

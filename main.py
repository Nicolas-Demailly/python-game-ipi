import screen, time, sys;
from activity import menu; #, test;

sys.stderr = open('log.txt', 'w')

#test.init();
menu.init();

while menu.running:
    start = int(round(time.time() * 1000))
    for mod in screen.getModules():
        if "tick" in mod['functions']:
            mod['functions']['tick'](mod);
    screen.draw();
    delay = int(round(time.time() * 1000)) - start

import subprocess, sys, module, time, config
from multiprocessing import Process

display = True;

pixel = {
    None : ['\33[107m', '\33[97m'],
    "0" : ['\33[40m', '\33[30m'],
    "1" : ['\33[41m', '\33[31m'],
    "2" : ['\33[42m', '\33[32m'],
    "3" : ['\33[43m', '\33[33m'],
    "4" : ['\33[44m', '\33[34m'],
    "5" : ['\33[45m', '\33[35m'],
    "6" : ['\33[46m', '\33[36m'],
    "7" : ['\33[47m', '\33[37m'],
    "8" : ['\33[100m', '\33[90m'],
    "9" : ['\33[101m', '\33[91m'],
    "A" : ['\33[102m', '\33[92m'],
    "B" : ['\33[103m', '\33[93m'],
    "C" : ['\33[104m', '\33[94m'],
    "D" : ['\33[105m', '\33[95m'],
    "E" : ['\33[106m', '\33[96m'],
    "F" : ['\33[107m', '\33[97m']
}

fps = float(config.getData('fps'));
threads = 4;

modules = [];

osSize = [0, 0]; # 
size = {
    'osX' : int(osSize[0]),
    'osY' : int(osSize[1])
};

#factor = 3.5;

#if (size['osY'] / size['osX']) >= (16 / 9):
#    size['gameY'] = size['osX'] / (factor * 2);
#    size['gameX'] = (size['gameY'] / 9) * 32; 
#else:
#    size['gameX'] = size['osY'] / factor;
#    size['gameY'] = (size['gameX'] / 32) * 9;

size['gameX'] = 219; #int(size['gameX']);
size['gameY'] = 61; #int(size['gameY']);

sys.stdout.write("\033[8;{};{}t".format(size['gameY'], size['gameX']));
sys.stdout.write("\033c");
for y in range(0, size['gameY']):
    l, l1 = [], [];
    for x in range(0, size['gameX']):
        sys.stdout.write(" ");
sys.stdout.flush();
         
def getOSScreenSize():
    global size;
    return [size['osX'], size['osY']];

def getGameScreenSize():
    global size;
    return [size['gameX'], size['gameY'] * 2];

def getModules():
    global modules;
    return modules;

def addModule(module):
    global modules;
    modules.append(module);

def removeModule(module):
    global modules;
    if module in modules:
        modules.remove(module);

def containsModule(module):
    global modules;
    return module in modules;

def clearModule():
    global modules;
    modules.clear();

def drawProcess(modules, start, stop):
    global sys, size, display
    for y in range(start, stop):
        for x in range(0, size['gameX']):
            realY = y*2;
            pixelTop = None;
            pixelBottom = None;
            for mod in modules:
                if not "hidden" in mod or mod["hidden"] == False:
                    if pixelTop == None:
                        pixelTop = module.getPixel(mod, x, realY);
                    if pixelBottom == None:
                        pixelBottom = module.getPixel(mod, x, realY+1);
                    if not (pixelTop == None or pixelBottom == None):
                        break;
            if display:
                sys.stdout.write("\033[{};{}H{}{}▄".format(y+1, x+1, pixel[pixelTop][0], pixel[pixelBottom][1]));

def draw():
    global sys, modules, threads, size
    deep = list(modules);
    deep.reverse();
    
    l = [];
    
    start = 0;
    stop = 0;
    for i in range(0, threads):
        stop = stop + int(size['gameY'] / 4);
        if (i == threads - 1):
            stop = size['gameY'];
        t = Process(target = drawProcess, args = (deep, start, stop))
        t.start();
        l.append(t);
        start = stop;
    
    time.sleep(1. / fps);
    for a in l:
        a.terminate()
        a.join()
    
    sys.stdout.flush();
    